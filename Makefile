deploy-stage:
	git push
	git push staging master
	heroku run --app stage-rpg-dice-roller rails db:migrate

migrate-prod:
	heroku run --app rpg-dice-roller rake db:migrate

logs-stage:
	heroku logs --app stage-rpg-dice-roller --tail

logs-prod:
	heroku logs --app rpg-dice-roller --tail

db-export-stage:
	heroku pg:backups capture
	curl -o ../dump/rpg-dice-roller-stage.dump `heroku pg:backups public-url`

db-export-prod:
	heroku pg:backups capture --app rpg-dice-roller
	curl -o ../dump/rpg-dice-roller-prod.dump `heroku pg:backups public-url --app rpg-dice-roller`

db-import-from-prod:
	heroku pg:backups capture --app rpg-dice-roller
	curl -o ../dump/rpg-dice-roller-prod.dump `heroku pg:backups public-url --app rpg-dice-roller`
	docker-compose exec -d db bash -c "pg_restore -d dice_roller_development -c -U postgres docker-entrypoint-initdb.d/rpg-dice-roller-prod.dump"
	docker-compose exec web rails db:migrate
