# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#

# Clear dice form.
$(document).on 'click', '.polyhedron form .clear', (event) ->
  $('input[type=number]').val(0)
  $('input#tag').val('')
  return false

$(document).ready ->
  clip = new ZeroClipboard($('#rubbcode_clip_button'))
  clip = new ZeroClipboard($('#bbcode_clip_button'))
