# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#

# Clear dice form.
$(document).on 'click', '.genesys form .clear', (event) ->
  $('input#ability').val(0)
  $('input#proficiency').val(0)
  $('input#difficulty').val(0)
  $('input#challenge').val(0)
  $('input#boost').val(0)
  $('input#setback').val(0)
  $('input#tag').val('')
  return false

$(document).ready ->
  clip = new ZeroClipboard($('#rubbcode_clip_button'))
  clip = new ZeroClipboard($('#bbcode_clip_button'))
