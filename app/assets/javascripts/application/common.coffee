namespace = (target, name, block) ->
  [target, name, block] = [(if typeof exports isnt 'undefined' then exports else window), arguments...] if arguments.length < 3
  top    = target
  target = target[item] or= {} for item in name.split '.'
  block target, top

namespace 'DiceRoller', (exports) ->
  log = []
  system = ''
  $(document).on 'turbolinks:load', ->
    system = $('body').data('system')

  # Increase dice count.
  $(document).on 'click', '.increase', ->
    dice = $(this).closest('.input-group').attr 'data-dice'
    num = $('input#' + dice).val()
    $('input#' + dice).val(parseInt(num) + 1)

  # Decrease dice count.
  $(document).on 'click', '.decrease', ->
    dice = $(this).closest('.input-group').attr 'data-dice'
    num = $('input#' + dice).val()
    if !$('input#' + dice).attr('min') || num > $('input#' + dice).attr('min')
      $('input#' + dice).val(num - 1)

  # Submit dice form.
  $(document).on 'click', 'form input[type=submit]', (event) ->
    $('#dice-result').hide()
    $('#computed-result').hide()
    $('#dice-rolling').show()
    $("#roll-log-table tr.selected").removeClass('selected')
    $('html, body').animate { scrollTop: 0 }, 'fast'

  # Initialize log table.
  $(document).on 'turbolinks:load', ->
    log = JSON.parse(localStorage.getItem('roll_log_'+system)) || []
    last_log = log[log.length - 1]
    dataSet = []

    i = log.length - 1
    while i >= 0
      dataSet.push [
        DiceRoller.formatLogDate(log[i].time)
        log[i].diceText
        log[i].resultText
        log[i].tag
        "<div class='btn-group' role='group' data-dice-rolled='" + log[i].diceText + "' data-log-id='" + i + "'><button class='btn btn-sm btn-success reroll-from-log'>Reroll</button><button class='btn btn-sm btn-danger delete-from-log'>Delete</button></div>"
      ]
      i--

    table = $('#roll-log-table').DataTable(
      'createdRow': (row, data, dataIndex) ->
        $(row).attr 'data-date', data[0]
        $(row).attr 'data-system', system
      'data': dataSet,
      'stateSave': true,
      'pageLength': 50,
      'order': [[ 0, 'desc' ]]
      'columnDefs': [ { 'orderable': true, "targets": 0 }, { 'orderable': false, "targets": [1,2,3,4] } ],
    )

  # Clear Roll log.
  $(document).on 'click', '#clearLog', (event) ->
    localStorage.setItem('roll_log_'+system, JSON.stringify([]));
    t = $('#roll-log-table').DataTable();
    t.clear().draw()
    alert('Roll log cleared.')
    event.preventDefault()

  # Delete single entry from log.
  $(document).on 'click', 'button.delete-from-log', (event) ->
    log = JSON.parse(localStorage.getItem('roll_log_'+system)) || []
    t = $('#roll-log-table').DataTable();
    t.row($(this).parents('tr')).remove().draw()
    button = $(this)
    logRow = button.closest('.btn-group').data('log-id')
    log.splice(logRow, 1)
    localStorage.setItem('roll_log_' + system, JSON.stringify(log))

  # Reroll from log.
  $(document).on 'click', 'button.reroll-from-log', (event) ->
    $('html, body').animate { scrollTop: 0 }, 'fast'
    $('#' + system + '-roll')[0].reset();
    button = $(this)
    diceRolled = button.closest('.btn-group').data('dice-rolled')
    if system == 'polyhedron'
      for dice in diceRolled.split '<br>'
        dice_vars = dice.split(/^([0-9])D(\d{1,3})(-|\+)*(\d+)*$/)
        numDice = parseInt(dice_vars[1])
        typeDice = dice_vars[2]
        modifier = 0
        if (parseInt(dice_vars[3] + dice_vars[4]))
          modifier = parseInt(dice_vars[3] + dice_vars[4])
        $('#d' + typeDice.toLowerCase()).val(numDice)
        $('#d' + typeDice.toLowerCase() + 'mod').val(modifier)
    else
      for dice in diceRolled.split ', '
        numDice = dice.split(' ')[0]
        typeDice = dice.split(' ')[1]
        $('#' + typeDice.toLowerCase()).val(numDice)
    $('#' + system + '-roll input[type=submit]').click()


  forceTwoDigits = (val) ->
    if val < 10
      return "0#{val}"
    return val

  exports.formatLogDate = (timestamp) ->
    date = new Date(timestamp * 1000)
    year = date.getFullYear()
    month = forceTwoDigits(date.getMonth()+1)
    day = forceTwoDigits(date.getDate())
    hour = forceTwoDigits(date.getHours())
    minute = forceTwoDigits(date.getMinutes())
    second = forceTwoDigits(date.getSeconds())
    formattedTime = "#{year}-#{month}-#{day} #{hour}:#{minute}:#{second}"

  exports.logLimit = ->
    return 2000