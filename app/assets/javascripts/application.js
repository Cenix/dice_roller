// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require datatables.min
//= require dataTables.select.min
//= require zeroclipboard
//= require application/common
//= require genesys
//= require star_wars
//= require xwing
//= require polyhedron
//= require application

//$(function(){ $(document).foundation(); });
//= require cookies_eu

$(function() {
  var loc = window.location.href;
  if(loc.includes('embed=modal')) {
    $('body').addClass('modal-embedded');
  }

});