module ApplicationHelper

  def current_class?(test_path)
    return 'active' if request.path == test_path
    ''
  end

  def current_page(page_icon)
    @page == page_icon
  end

  def parent_page(page_icon)
    @parent_page == page_icon
  end

  def one_line(&block)
    if capture_haml(&block)
      haml_concat capture_haml(&block).gsub("\n", '').gsub('\\n', "\n").html_safe
    end
  end

  def get_dice_text(dice_rolled)
    return if dice_rolled.nil?
    dice_rolled.symbolize_keys!
    dice_text = []
    dice_text << "#{dice_rolled[:a]} Ability" if dice_rolled[:a]
    dice_text << "#{dice_rolled[:p]} Proficiency" if dice_rolled[:p]
    dice_text << "#{dice_rolled[:d]} Difficulty" if dice_rolled[:d]
    dice_text << "#{dice_rolled[:c]} Challenge" if dice_rolled[:c]
    dice_text << "#{dice_rolled[:b]} Boost" if dice_rolled[:b]
    dice_text << "#{dice_rolled[:s]} Setback" if dice_rolled[:s]
    dice_text << "#{dice_rolled[:f]} Force" if dice_rolled[:f]

    dice_text
  end

  def get_result_text(dice_results)
    successes = 0
    advantages = 0
    triumphs = 0
    failures = 0
    threats = 0
    despairs = 0
    blacks = 0
    whites = 0
    hits = 0
    crits = 0
    evades = 0
    focuses = 0

    dice_results.each do |result|
      result.symbolize_keys!
      result[:faces].each do |face|
        case face[0].to_sym
        when :success
          successes += face[1]
        when :advantage
          advantages += face[1]
        when :triumph
          triumphs += face[1]
        when :failure
          failures += face[1]
        when :threat
          threats += face[1]
        when :despair
          despairs += face[1]
        when :black
          blacks += face[1]
        when :white
          whites += face[1]
        when :hit
          hits += face[1]
        when :crit
          crits += face[1]
        when :evade
          evades += face[1]
        when :focus
          focuses += face[1]
        end
      end
    end

    # Add triumphs to sucesses.
    successes += triumphs

    # Add despairs to failures.
    failures += despairs

    # Subtract failures from successes.
    failures.times do
      break if successes == 0
      successes -= 1
      failures -= 1
    end

    # Subtract threats from advantages.
    threats.times do
      break if advantages == 0
      advantages -= 1
      threats -= 1
    end

    result_text = []
    result_text << pluralize(successes, 'success') if successes > 0
    result_text << pluralize(advantages, 'advantage') if advantages > 0
    result_text << "#{pluralize(triumphs, 'triumph')}*" if triumphs > 0
    result_text << pluralize(failures, 'failure') if failures > 0
    result_text << pluralize(threats, 'threat') if threats > 0
    #result_text << pluralize(despairs, 'despair') if despairs > 0
    result_text << "#{pluralize(despairs, 'despair')}**" if despairs > 0
    result_text << "#{whites} Light Side" if whites > 0
    result_text << "#{blacks} Dark Side" if blacks > 0
    result_text << pluralize(hits, 'hit') if hits > 0
    result_text << pluralize(crits, 'crit') if crits > 0
    result_text << pluralize(evades, 'evade') if evades > 0
    result_text << pluralize(focuses, 'focus') if focuses > 0
    result_text
  end

  def trim_log
    trim_date = 14.days.ago
    roll_log = Roll.where("created_at < ?", trim_date)

    roll_log.each do |log|
      log.destroy
    end
  end
end
