class XwingController < ApplicationController
  include ApplicationHelper
  include ActionView::Helpers::TextHelper

  before_action :set_page
  before_action :initialize_dice

  def index
    embedded = params['embed'].present?

    @dice_results = []
    @hits = 0
    @crits = 0
    @evades = 0
    @focuses = 0

    @dice_rolled = {
      a: params['a'].present? ? params['a'] : 0,
      d: params['d'].present? ? params['d'] : 0,
      keep: params['keep'].present? ? params['keep'] : 'true',
      log: params['log'].present? ? params['log'] : 'true',
      tag: params['tag'].present? ? params['tag'] : '',
      embedded: embedded
    }

    respond_to do |format|
      format.html
    end
  end

  def roll
    if params[:commit].present?
      @roll_params = {
        a: params[:a].to_i || 0,
        d: params[:d].to_i || 0,
      }

      roll_check = @roll_params.values.inject { |a, b| a.to_i  + b.to_i }
      if roll_check > 0
        @roll_params = @roll_params.delete_if { |_key, value| value.to_i < 1 }

        @dice_result = get_dice_results(@roll_params)
        @results = get_results(@dice_result)
        @raw_result_text = get_raw_result_text(@dice_result)
        @result_text = get_result_text(@dice_result)
        @roll_tag = params[:tag] || ''

        if (params[:persist])
          @log_date = Time.now.to_i
          @log_dice_text = get_dice_text(@roll_params.symbolize_keys).join(', ') unless @roll_params.nil?
          if get_result_text(@dice_result).blank?
            @log_result_text = "*Wash*"
          else
            @log_result_text = get_result_text(@dice_result).join(', ')
          end
        end
      end
    end
  end

  private

  def get_dice_results(roll_params)
    dice_results = []

    roll_params.each do |roll|
      roll[1].to_i.times do
        case roll[0]
        when :a
          dice = @attack_dice
        when :d
          dice = @defense_dice
        end

        icon = []
        if dice == 'd100'
          dice_name = 'd100'
          side = rand(1..100)
          faces = []#[*1..100]
          icon = ['d100']
        else
          dice_name = dice.name
          side = rand(1..dice.num_sides)
          faces = dice.send("side#{side}")

          faces.each do |face|
            face[1].times do
              icon << face[0].to_s
            end
          end
        end

        dice_results << {
          dice: dice_name,
          side: side,
          faces: faces,
          icon: icon.any? ? icon.join('-') : 'blank'
        }
      end
    end

    dice_results
  end

  def get_raw_result_text(dice_results)
    hits = 0
    crits = 0
    evades = 0
    focuses = 0

    dice_results.each do |result|
      result.symbolize_keys!
      result[:faces].each do |face|
        case face[0].to_sym
        when :hit
          hits += face[1]
        when :crit
          crits += face[1]
        when :evade
          evades += face[1]
        when :focus
          focuses += face[1]
        end
      end
    end

    result_text = []
    result_text << pluralize(hits, 'hit') if hits > 0
    result_text << pluralize(crits, 'crit') if crits > 0
    result_text << pluralize(evades, 'evade') if evades > 0
    result_text << pluralize(focuses, 'focus') if focuses > 0
    result_text
  end

  def get_results(dice_results)
    results = {
      hits: 0,
      crits: 0,
      evades: 0,
      focuses: 0,
    }

    dice_results.each do |result|
      result.symbolize_keys!
      result[:faces].each do |face|
        case face[0].to_sym
        when :hit
          results[:hits] += face[1]
        when :crit
          results[:crits] += face[1]
        when :evade
          results[:evades] += face[1]
        when :focus
          results[:focuses] += face[1]
        end
      end
    end

    results
  end

  def initialize_dice
    @attack_dice = XwingAttackDice.new
    @defense_dice = XwingDefenseDice.new
  end

  def set_page
    @page = 'xwing'
    @parent_page = 'rollers'
    @system = 'xwing'
  end
end
