class PolyhedronController < ApplicationController
  include ApplicationHelper
  include ActionView::Helpers::TextHelper

  before_action :set_page
  #before_action :initialize_dice

  def index
    embedded = params['embed'].present?

    @dice_results = []

    @dice_rolled = {
      d4: params['d4'].present? ? params['d4'] : 0,
      d4mod: params['d4mod'].present? ? params['d4mod'] : 0,
      d6: params['d6'].present? ? params['d6'] : 0,
      d6mod: params['d6mod'].present? ? params['d6mod'] : 0,
      d8: params['d8'].present? ? params['d8'] : 0,
      d8mod: params['d8mod'].present? ? params['d8mod'] : 0,
      d10: params['d10'].present? ? params['d10'] : 0,
      d10mod: params['d10mod'].present? ? params['d10mod'] : 0,
      d12: params['d12'].present? ? params['d12'] : 0,
      d12mod: params['d12mod'].present? ? params['d12mod'] : 0,
      d20: params['d20'].present? ? params['d20'] : 0,
      d20mod: params['d20mod'].present? ? params['d20mod'] : 0,
      d100: params['d100'].present? ? params['d100'] : 0,
      d100mod: params['d100mod'].present? ? params['d100mod'] : 0,
      keep: params['keep'].present? ? params['keep'] : 'true',
      log: params['log'].present? ? params['log'] : 'true',
      tag: params['tag'].present? ? params['tag'] : '',
      embedded: embedded
    }

    respond_to do |format|
      format.html
    end
  end

  def roll
    if params[:commit].present?
      @roll_params_dice = {
        d4: params[:d4].to_i || 0,
        d6: params[:d6].to_i || 0,
        d8: params[:d8].to_i || 0,
        d10: params[:d10].to_i || 0,
        d12: params[:d12].to_i || 0,
        d20: params[:d20].to_i || 0,
        d100: params[:d100].to_i || 0,
      }
      @roll_params_mods = {
        d4mod: params[:d4mod].to_i || 0,
        d6mod: params[:d6mod].to_i || 0,
        d8mod: params[:d8mod].to_i || 0,
        d10mod: params[:d10mod].to_i || 0,
        d12mod: params[:d12mod].to_i || 0,
        d20mod: params[:d20mod].to_i || 0,
        d100mod: params[:d100mod].to_i || 0,
      }

      @roll_log = []

      roll_check = @roll_params_dice.values.inject { |a, b| a.to_i  + b.to_i }
      if roll_check > 0
        @dice_result = get_dice_results(@roll_params_dice, @roll_params_mods)
        @results = @dice_result
        @result_text = []
        @roll_tag = params[:tag] || ''

        @log_dice_text = ''
        @log_result_text = ''
        if (params[:persist])
          @log_date = Time.now.to_i
          log_dice_text = []
          log_result_text = []
          @results.each do |result|
            next if result[1][:rolls].blank?
            log_dice_text << "#{result[1][:rolls].count}#{result[0].upcase}#{sprintf("%+d", result[1][:modifier]) if result[1][:modifier] != 0}"
            log_result_text << result[1][:total]
          end
          @log_dice_text = log_dice_text.to_sentence(words_connector: "<br>", last_word_connector: "<br>", two_words_connector: '<br>') || ''
          @log_result_text = log_result_text.to_sentence(words_connector: "<br>", last_word_connector: "<br>", two_words_connector: '<br>') || ''
        end
      end
    end
  end

  private

  def get_dice_results(roll_params_dice, roll_params_mods)
    dice_results = {
      d4: { rolls: [], modifier: roll_params_mods[:d4mod], total: 0, icon: 'd4' },
      d6: { rolls: [], modifier: roll_params_mods[:d6mod], total: 0, icon: 'd6' },
      d8: { rolls: [], modifier: roll_params_mods[:d8mod], total: 0, icon: 'd8' },
      d10: { rolls: [], modifier: roll_params_mods[:d10mod], total: 0, icon: 'd1' },
      d12: { rolls: [], modifier: roll_params_mods[:d12mod], total: 0, icon: 'd12' },
      d20: { rolls: [], modifier: roll_params_mods[:d20mod], total: 0, icon: 'd20' },
      d100: { rolls: [], modifier: roll_params_mods[:d100mod], total: 0, icon: 'd100' },
    }

    roll_params_dice[:d4].to_i.times do
      dice_results[:d4][:rolls] << rand(1..4)
    end
    dice_results[:d4][:total] = dice_results[:d4][:rolls].sum + dice_results[:d4][:modifier]

    roll_params_dice[:d6].to_i.times do
      dice_results[:d6][:rolls] << rand(1..6)
    end
    dice_results[:d6][:total] = dice_results[:d6][:rolls].sum + dice_results[:d6][:modifier]

    roll_params_dice[:d8].to_i.times do
      dice_results[:d8][:rolls] << rand(1..8)
    end
    dice_results[:d8][:total] = dice_results[:d8][:rolls].sum + dice_results[:d8][:modifier]

    roll_params_dice[:d10].to_i.times do
      dice_results[:d10][:rolls] << rand(1..10)
    end
    dice_results[:d10][:total] = dice_results[:d10][:rolls].sum + dice_results[:d10][:modifier]

    roll_params_dice[:d12].to_i.times do
      dice_results[:d12][:rolls] << rand(1..12)
    end
    dice_results[:d12][:total] = dice_results[:d12][:rolls].sum + dice_results[:d12][:modifier]

    roll_params_dice[:d20].to_i.times do
      dice_results[:d20][:rolls] << rand(1..20)
    end
    dice_results[:d20][:total] = dice_results[:d20][:rolls].sum + dice_results[:d20][:modifier]

    roll_params_dice[:d100].to_i.times do
      dice_results[:d100][:rolls] << rand(1..100)
    end
    dice_results[:d100][:total] = dice_results[:d100][:rolls].sum + dice_results[:d100][:modifier]

    dice_results
  end

  def set_page
    @page = 'polyhedron_rpg'
    @parent_page = 'rollers'
    @system = 'polyhedron'
  end

end
