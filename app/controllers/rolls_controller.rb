class RollsController < ApplicationController
  before_action :authenticate_admin!, except: [:index, :truncate_log]
  before_action :set_roll, only: [:show, :edit, :update, :destroy]

  # GET /rolls
  # GET /rolls.json
  def index
    if params[:uuid]
      @rolls = Roll.where(uuid: params[:uuid]).order(created_at: :desc).page params[:page]
    else
      @rolls = Roll.order(created_at: :desc).page params[:page]
    end
  end

  # POST /rolls
  # POST /rolls.json
  def create
    @roll = Roll.new(roll_params)

    respond_to do |format|
      if @roll.save
        format.html { redirect_to @roll, notice: 'Roll was successfully created.' }
        format.json { render :show, status: :created, location: @roll }
      else
        format.html { render :new }
        format.json { render json: @roll.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rolls/1
  # PATCH/PUT /rolls/1.json
  def update
    respond_to do |format|
      if @roll.update(roll_params)
        format.html { redirect_to @roll, notice: 'Roll was successfully updated.' }
        format.json { render :show, status: :ok, location: @roll }
      else
        format.html { render :edit }
        format.json { render json: @roll.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rolls/1
  # DELETE /rolls/1.json
  def destroy
    @roll.destroy
    respond_to do |format|
      format.html { redirect_to rolls_url, notice: 'Roll was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def trim_log
    @trim_date = 14.days.ago
    @count = 0

    roll_log = Roll.where("created_at < ?", @trim_date)

    roll_log.each do |log|
      @count += 1
      log.destroy
    end

    flash[:notice] = "Trimmed #{@count} dice logs from before: #{Time.at(@trim_date).to_datetime.in_time_zone}"
    redirect_to rolls_url
  end

  def truncate_log
    ActiveRecord::Base.connection.execute("TRUNCATE rolls")
    flash[:notice] = "Cleared all dice roll logs."
    redirect_to rolls_url
  end

  def geoloc
    rolls = Roll.all

    rolls.each do |roll|
      roll.geoloc = roll.origin_of_roll
      roll.save
    end
    redirect_to rolls_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_roll
      @roll = Roll.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def roll_params
      params.require(:roll).permit(:ability, :difficulty, :proficiency, :challenge, :boost, :setback, :dice_result, :results, :raw_result_text, :result_text, :roll_params, :ip, :uuid, :system)
    end
end
