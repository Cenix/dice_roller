class StarWarsController < ApplicationController
  include ApplicationHelper
  include ActionView::Helpers::TextHelper

  before_action :set_page
  before_action :initialize_dice

  def index
    embedded = params['embed'].present?

    @dice_results = []
    @successes = 0
    @advantages = 0
    @triumphs = 0
    @failures = 0
    @threats = 0
    @despairs = 0
    @blacks = 0
    @whites = 0

    @dice_rolled = {
      a: params['a'].present? ? params['a'] : 0,
      d: params['d'].present? ? params['d'] : 0,
      p: params['p'].present? ? params['p'] : 0,
      c: params['c'].present? ? params['c'] : 0,
      b: params['b'].present? ? params['b'] : 0,
      s: params['s'].present? ? params['s'] : 0,
      f: params['f'].present? ? params['f'] : 0,
      keep: params['keep'].present? ? params['keep'] : 'true',
      log: params['log'].present? ? params['log'] : 'true',
      tag: params['tag'].present? ? params['tag'] : '',
      embedded: embedded
    }

    respond_to do |format|
      format.html
    end
  end

  def roll
    if params[:commit].present?
      @roll_params = {
        a: params[:a].to_i || 0,
        d: params[:d].to_i || 0,
        p: params[:p].to_i || 0,
        c: params[:c].to_i || 0,
        b: params[:b].to_i || 0,
        s: params[:s].to_i || 0,
        f: params[:f].to_i || 0,
        percent: params[:percent].to_i || 0,
      }

      @roll_log = []

      roll_check = @roll_params.values.inject { |a, b| a.to_i  + b.to_i }
      if roll_check > 0
        @roll_params = @roll_params.delete_if { |_key, value| value.to_i < 1 }

        @dice_result = get_dice_results(@roll_params)
        @results = get_results(@dice_result)
        @raw_result_text = get_raw_result_text(@dice_result)
        @result_text = get_result_text(@dice_result)
        @roll_tag = params[:tag] || ''

        if (params[:persist])
          @log_date = Time.now.to_i
          @log_dice_text = get_dice_text(@roll_params.symbolize_keys).join(', ') unless @roll_params.nil?
          if get_result_text(@dice_result).blank?
            @log_result_text = "*Wash*"
          else
            @log_result_text = get_result_text(@dice_result).join(', ')
          end
        end
      end
    end
  end

  private

  def get_dice_text(dice_rolled)
    return if dice_rolled.nil?
    dice_rolled.symbolize_keys!
    dice_text = []
    dice_text << "#{dice_rolled[:a]} Ability" if dice_rolled[:a]
    dice_text << "#{dice_rolled[:p]} Proficiency" if dice_rolled[:p]
    dice_text << "#{dice_rolled[:d]} Difficulty" if dice_rolled[:d]
    dice_text << "#{dice_rolled[:c]} Challenge" if dice_rolled[:c]
    dice_text << "#{dice_rolled[:b]} Boost" if dice_rolled[:b]
    dice_text << "#{dice_rolled[:s]} Setback" if dice_rolled[:s]
    dice_text << "#{dice_rolled[:f]} Force" if dice_rolled[:f]

    dice_text
  end

  def get_dice_results(roll_params)
    dice_results = []

    roll_params.each do |roll|
      roll[1].to_i.times do
        case roll[0]
        when :a
          dice = @ability_dice
        when :d
          dice = @difficulty_dice
        when :p
          dice = @proficiency_dice
        when :c
          dice = @challenge_dice
        when :b
          dice = @boost_dice
        when :s
          dice = @setback_dice
        when :f
          dice = @force_dice
        when :percent
          dice = 'd100'
        end

        icon = []
        if dice == 'd100'
          dice_name = 'd100'
          side = rand(1..100)
          faces = []#[*1..100]
          icon = ['d100']
        else
          dice_name = dice.name
          side = rand(1..dice.num_sides)
          faces = dice.send("side#{side}")

          faces.each do |face|
            face[1].times do
              icon << face[0].to_s
            end
          end
        end

        dice_results << {
          dice: dice_name,
          side: side,
          faces: faces,
          icon: icon.any? ? icon.join('-') : 'blank'
        }
      end
    end

    dice_results
  end

  def get_raw_result_text(dice_results)
    successes = 0
    advantages = 0
    triumphs = 0
    failures = 0
    threats = 0
    despairs = 0
    blacks = 0
    whites = 0

    dice_results.each do |result|
      result.symbolize_keys!
      result[:faces].each do |face|
        case face[0].to_sym
        when :success
          successes += face[1]
        when :advantage
          advantages += face[1]
        when :triumph
          triumphs += face[1]
        when :failure
          failures += face[1]
        when :threat
          threats += face[1]
        when :despair
          despairs += face[1]
        when :black
          blacks += face[1]
        when :white
          whites += face[1]
        end
      end
    end

    result_text = []
    result_text << pluralize(successes, 'success') if successes > 0
    result_text << pluralize(advantages, 'advantage') if advantages > 0
    result_text << pluralize(triumphs, 'triumph') if triumphs > 0
    result_text << pluralize(failures, 'failure') if failures > 0
    result_text << pluralize(threats, 'threat') if threats > 0
    result_text << pluralize(despairs, 'despair') if despairs > 0
    result_text << "#{whites} Light Side" if whites > 0
    result_text << "#{blacks} Dark Side" if blacks > 0
    result_text
  end

  def get_results(dice_results)
    results = {
      successes: 0,
      advantages: 0,
      triumphs: 0,
      failures: 0,
      threats: 0,
      despairs: 0,
      blacks: 0,
      whites: 0,
    }

    dice_results.each do |result|
      result.symbolize_keys!
      result[:faces].each do |face|
        case face[0].to_sym
        when :success
          results[:successes] += face[1]
        when :advantage
          results[:advantages] += face[1]
        when :triumph
          results[:triumphs] += face[1]
        when :failure
          results[:failures] += face[1]
        when :threat
          results[:threats] += face[1]
        when :despair
          results[:despairs] += face[1]
        when :black
          results[:blacks] += face[1]
        when :white
          results[:whites] += face[1]
        end
      end
    end

    # Add triumphs to sucesses.
    results[:successes] += results[:triumphs]

    # Add despairs to failures.
    results[:failures] += results[:despairs]

    # Subtract failures from successes.
    results[:failures].times do
      break if results[:successes] == 0
      results[:successes] -= 1
      results[:failures] -= 1
    end

    # Subtract threats from advantages.
    results[:threats].times do
      break if results[:advantages] == 0
      results[:advantages] -= 1
      results[:threats] -= 1
    end

    results
  end

  def get_result_text(dice_results)
    successes = 0
    advantages = 0
    triumphs = 0
    failures = 0
    threats = 0
    despairs = 0
    blacks = 0
    whites = 0

    dice_results.each do |result|
      result.symbolize_keys!
      result[:faces].each do |face|
        case face[0].to_sym
        when :success
          successes += face[1]
        when :advantage
          advantages += face[1]
        when :triumph
          triumphs += face[1]
        when :failure
          failures += face[1]
        when :threat
          threats += face[1]
        when :despair
          despairs += face[1]
        when :black
          blacks += face[1]
        when :white
          whites += face[1]
        end
      end
    end

    # Add triumphs to sucesses.
    successes += triumphs

    # Add despairs to failures.
    failures += despairs

    # Subtract failures from successes.
    failures.times do
      break if successes == 0
      successes -= 1
      failures -= 1
    end

    # Subtract threats from advantages.
    threats.times do
      break if advantages == 0
      advantages -= 1
      threats -= 1
    end

    result_text = []
    result_text << pluralize(successes, 'success') if successes > 0
    result_text << pluralize(advantages, 'advantage') if advantages > 0
    result_text << "#{pluralize(triumphs, 'triumph')}*" if triumphs > 0
    result_text << pluralize(failures, 'failure') if failures > 0
    result_text << pluralize(threats, 'threat') if threats > 0
    result_text << "#{pluralize(despairs, 'despair')}**" if despairs > 0
    result_text << "#{whites} Light Side" if whites > 0
    result_text << "#{blacks} Dark Side" if blacks > 0
    result_text
  end

  def initialize_dice
    @ability_dice = AbilityDice.new
    @proficiency_dice = ProficiencyDice.new
    @difficulty_dice = DifficultyDice.new
    @challenge_dice = ChallengeDice.new
    @boost_dice = BoostDice.new
    @setback_dice = SetbackDice.new
    @force_dice = ForceDice.new
  end

  def set_page
    @page = 'star_wars_rpg'
    @parent_page = 'rollers'
    @system = 'star_wars'
  end
end
