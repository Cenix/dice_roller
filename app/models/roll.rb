class Roll < ActiveRecord::Base
  #paginates_per 10

  serialize :roll_params
  serialize :dice_result
  serialize :raw_result_text
  serialize :results
  serialize :result_text
  serialize :geoloc

  def origin_of_roll
    require 'net/http'

    origin = []
    ip_lookup = JSON.parse(Net::HTTP.get_response(URI.parse('http://ipinfo.io/' + ip )).body)

    unless ip_lookup['city'].blank?
      origin << ip_lookup['city']
    end
    unless ip_lookup['region'].blank?
      origin << ip_lookup['region']
    end
    unless ip_lookup['country'].blank?
      origin << ip_lookup['country']
    end

    origin
  end

end
