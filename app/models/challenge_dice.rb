# Setup for the Challenge Dice.
class ChallengeDice
  def num_sides
    12
  end

  def name
    'challenge'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
      threat: 1
    }
    states
  end

  def side3
    states = {
      threat: 1
    }
    states
  end

  def side4
    states = {
      despair: 1
    }
    states
  end

  def side5
    states = {
      failure: 1
    }
    states
  end

  def side6
    states = {
      failure: 1
    }
    states
  end

  def side7
    states = {
      threat: 2
    }
    states
  end

  def side8
    states = {
      threat: 2
    }
    states
  end

  def side9
    states = {
      failure: 1,
      threat: 1
    }
    states
  end

  def side10
    states = {
      failure: 1,
      threat: 1
    }
    states
  end

  def side11
    states = {
      failure: 2
    }
    states
  end

  def side12
    states = {
      failure: 2
    }
    states
  end
end
