# Setup for the SW RPG Force Dice.
class ForceDice
  def num_sides
    12
  end

  def name
    'force'
  end

  def side1
    states = {
      black: 1
    }
    states
  end

  def side2
    states = {
      black: 1
    }
    states
  end

  def side3
    states = {
      black: 1
    }
    states
  end

  def side4
    states = {
      black: 1
    }
    states
  end

  def side5
    states = {
      black: 1
    }
    states
  end

  def side6
    states = {
      black: 1
    }
    states
  end

  def side7
    states = {
      white: 1
    }
    states
  end

  def side8
    states = {
      white: 1
    }
    states
  end

  def side9
    states = {
      white: 2
    }
    states
  end

  def side10
    states = {
      white: 2
    }
    states
  end

  def side11
    states = {
      white: 2
    }
    states
  end

  def side12
    states = {
      black: 2
    }
    states
  end
end
