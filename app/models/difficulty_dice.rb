# Setup for the Difficulty Dice.
class DifficultyDice
  def num_sides
    8
  end

  def name
    'difficulty'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
      failure: 2
    }
    states
  end

  def side3
    states = {
      threat: 2
    }
    states
  end

  def side4
    states = {
      failure: 1,
      threat: 1
    }
    states
  end

  def side5
    states = {
      failure: 1
    }
    states
  end

  def side6
    states = {
      threat: 1
    }
    states
  end

  def side7
    states = {
      threat: 1
    }
    states
  end

  def side8
    states = {
      threat: 1
    }
    states
  end
end
