class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #devise :database_authenticatable, :registerable,
  #       :recoverable, :rememberable, :trackable, :validatable

  # Switch to this after first prod deploy and have created user.
  devise :database_authenticatable,
    :recoverable, :rememberable, :trackable, :validatable
end
