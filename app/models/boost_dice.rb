# Setup for the Boost Dice.
class BoostDice
  def num_sides
    6
  end

  def name
    'boost'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
    }
    states
  end

  def side3
    states = {
      advantage: 1
    }
    states
  end

  def side4
    states = {
      success: 1
    }
    states
  end

  def side5
    states = {
      success: 1,
      advantage: 1,
    }
    states
  end

  def side6
    states = {
      advantage: 2
    }
    states
  end
end