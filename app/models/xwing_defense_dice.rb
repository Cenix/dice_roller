# Setup for the X-Wing Minis Defense Dice.
class XwingDefenseDice
  def num_sides
    8
  end

  def name
    'defense'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
    }
    states
  end

  def side3
    states = {
    }
    states
  end

  def side4
    states = {
      focus: 1
    }
    states
  end

  def side5
    states = {
      focus: 1
    }
    states
  end

  def side6
    states = {
      evade: 1
    }
    states
  end

  def side7
    states = {
      evade: 1
    }
    states
  end

  def side8
    states = {
      evade: 1
    }
    states
  end
end
