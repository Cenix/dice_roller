class ProficiencyDice
  def num_sides
    12
  end

  def name
    'proficiency'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
      success: 2
    }
    states
  end

  def side3
    states = {
      success: 2
    }
    states
  end

  def side4
    states = {
      success: 1,
      advantage: 1
    }
    states
  end

  def side5
    states = {
      success: 1,
      advantage: 1
    }
    states
  end

  def side6
    states = {
      success: 1,
      advantage: 1
    }
    states
  end

  def side7
    states = {
      advantage: 2
    }
    states
  end

  def side8
    states = {
      advantage: 2
    }
    states
  end

  def side9
    states = {
      triumph: 1
    }
    states
  end

  def side10
    states = {
      success: 1
    }
    states
  end

  def side11
    states = {
      success: 1
    }
    states
  end

  def side12
    states = {
      advantage: 1
    }
    states
  end
end
