# Setup for the Ability Dice.
class AbilityDice
  def num_sides
    8
  end

  def name
    'ability'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
      success: 1
    }
    states
  end

  def side3
    states = {
      success: 1
    }
    states
  end

  def side4
    states = {
      advantage: 1
    }
    states
  end

  def side5
    states = {
      advantage: 1
    }
    states
  end

  def side6
    states = {
      advantage: 2
    }
    states
  end

  def side7
    states = {
      success: 2
    }
    states
  end

  def side8
    states = {
      success: 1,
      advantage: 1
    }
    states
  end
end
