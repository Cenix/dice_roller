# Setup for the X-Wing Minis Attack Dice.
class XwingAttackDice
  def num_sides
    8
  end

  def name
    'attack'
  end

  def side1
    states = {
    }
    states
  end

  def side2
    states = {
    }
    states
  end

  def side3
    states = {
      crit: 1
    }
    states
  end

  def side4
    states = {
      hit: 1
    }
    states
  end

  def side5
    states = {
      hit: 1
    }
    states
  end

  def side6
    states = {
      hit: 1
    }
    states
  end

  def side7
    states = {
      focus: 1
    }
    states
  end

  def side8
    states = {
      focus: 1
    }
    states
  end
end
