json.array!(@rolls) do |roll|
  json.extract! roll, :id, :ability, :difficulty, :proficiency, :challenge, :boost, :setback, :dice_result, :results, :raw_result_text, :result_text, :roll_params, :ip, :uuid, :system
  json.url roll_url(roll, format: :json)
end
