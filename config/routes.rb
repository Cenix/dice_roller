Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :admins

  # Star Wars routes
  get 'star_wars' => 'star_wars#index'
  get 'star_wars/roll' => redirect('star_wars')
  get 'star_wars/roll(/a:a)(/p:p)(/d:d)(/c:c)(/b:b)(/s:s)(/f:f)' => 'star_wars#roll'
  post 'star_wars/roll' => 'star_wars#roll', :defaults => { :format => 'js' }
  get 'star_wars/clear' => 'star_wars#clear_log'
  get 'star_wars/log' => 'star_wars#get_log'

  # Genesys routes
  get 'genesys' => 'genesys#index'
  get 'genesys/roll' => redirect('genesys')
  get 'api/v1/genesys/roll(/a:a)(/p:p)(/d:d)(/c:c)(/b:b)(/s:s)(/f:f)' => 'genesys#api_roll', :defaults => { :format => 'json' }
  post 'genesys/roll' => 'genesys#roll', :defaults => { :format => 'js' }
  get 'genesys/clear' => 'genesys#clear_log'
  get 'genesys/log' => 'genesys#get_log'

  # X-Wing routes
  get 'xwing' => 'xwing#index'
  get 'xwing/roll' => redirect('xwing')
  get 'xwing/roll(/a:a)(/p:p)(/d:d)(/c:c)(/b:b)(/s:s)(/f:f)' => 'xwing#roll'
  post 'xwing/roll' => 'xwing#roll', :defaults => { :format => 'js' }
  get 'xwing/clear' => 'xwing#clear_log'
  get 'xwing/log' => 'xwing#get_log'

  # Polyhedron routes
  get 'polyhedron' => 'polyhedron#index'
  get 'polyhedron/roll' => redirect('polyhedron')
  get 'api/v1/polyhedron/roll(/a:a)(/p:p)(/d:d)(/c:c)(/b:b)(/s:s)(/f:f)' => 'polyhedron#api_roll', :defaults => { :format => 'json' }
  post 'polyhedron/roll' => 'polyhedron#roll', :defaults => { :format => 'js' }
  get 'polyhedron/clear' => 'polyhedron#clear_log'
  get 'polyhedron/log' => 'polyhedron#get_log'


  get 'about' => 'pages#about'

  # Roll log routes.
  get 'rolls/trim' => 'rolls#trim_log'
  get 'rolls/truncate' => 'rolls#truncate_log'
  get 'rolls/geoloc' => 'rolls#geoloc'
  get 'rolls(/:uuid)' => 'rolls#index'
  resources :rolls

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'genesys#index'

end
