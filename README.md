# RPG Dice Roller

RPG Dice Roller is a simple dice roller app for popular tabletop games. At least that the plan but we all have to start somewhere, right?

This app is programmed using Rails 4.2.1 running on Ruby 2.2.1.

## Planned Features & Suggestions
Please use the [issues page](https://bitbucket.org/Cenix/dice_roller/issues).

## License
This work is licensed under a [CC BY-NC-SA International](http://creativecommons.org/licenses/by-nc-sa/4.0/) license. 

