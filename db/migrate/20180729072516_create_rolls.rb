class CreateRolls < ActiveRecord::Migration[5.2]
  def change
    create_table :rolls do |t|
      t.integer :ability
      t.integer :difficulty
      t.integer :proficiency
      t.integer :challenge
      t.integer :boost
      t.integer :setback
      t.string :dice_result
      t.string :results
      t.string :raw_result_text
      t.string :result_text
      t.string :roll_params
      t.string :ip
      t.string :uuid
      t.string :system

      t.timestamps null: false
    end
  end
end
