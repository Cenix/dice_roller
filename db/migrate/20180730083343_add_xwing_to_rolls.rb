class AddXwingToRolls < ActiveRecord::Migration[5.2]
  def change
    add_column :rolls, :attack, :integer
    add_column :rolls, :defense, :integer
  end
end
