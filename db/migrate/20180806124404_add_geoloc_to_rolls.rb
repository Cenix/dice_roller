class AddGeolocToRolls < ActiveRecord::Migration[5.2]
  def change
    add_column :rolls, :geoloc, :text
  end
end
