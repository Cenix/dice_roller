class AddForceToRolls < ActiveRecord::Migration[5.2]
  def change
    add_column :rolls, :force, :integer
  end
end
